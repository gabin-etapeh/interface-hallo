import { Component, OnInit, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AddressComponent } from './address/address.component';
import { AddressService } from './address.service';
import { ButtonsComponent } from './buttons/buttons.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet, 
    AddressComponent, 
    ButtonsComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {

  private service = inject(AddressService);

  globals: any;
  locations = [];

  ngOnInit() {
    this.service.getGeneralInformations()
      .subscribe(result => {
        this.globals = result;
        console.log(this.globals)
      });
    this.service.getAddresses()
      .subscribe(result => {
        this.locations = result.data;
        console.log(this.locations)
      });
  }
}
