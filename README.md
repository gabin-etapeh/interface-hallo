### HelloApp
Le projet consiste à réaliser une interface graphique avec le chargement des données de façon dynamique
#### Introduction
##### L'interface web est la suivante
<img src="src/assets/images/desktop.png" alt="Desktop" width="400"/>

##### L'interface mobile est la suivante
<img src="src/assets/images/mobile.png" alt="mobile" width="400"/>

#### Structure du projet
##### Le projet angular réalisé a la structure suivante:

##### node_modules: Contient les packages npm installés.
##### src: Contient le code source de l'application.
##### angular.json: Configuration globale du projet Angular.
##### package.json et package-lock.json: Fichiers de configuration des dépendances.

##### Dossier src :
app: Le dossier principal de l'application.
app.component.html, app.component.ts, app.component.css: Composant principal de l'application.
app.module.ts: Module racine de l'application.
assets: Contient les ressources statiques comme les images, les polices, etc.
environments: Configuration pour différents environnements de déploiement (ex: environment.prod.ts, environment.ts).
index.html: Point d'entrée de l'application.
styles.css: Fichier de styles globaux de l'application.

##### Autres Fichiers Importants :

angular.json: Configuration du projet Angular.
tsconfig.json: Configuration du transpileur TypeScript.
tslint.json: Fichier de configuration des règles de linting.

#### Comment lancer l'application
Exécutez ng servepour un serveur de développement. Aller vers http://localhost:4200/. L'application se rechargera automatiquement si vous modifiez l'un des fichiers source.
#### Durée de réalisation de la solution
J'ai réalisé ce travail en 16h étallées sur 5 jours

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
